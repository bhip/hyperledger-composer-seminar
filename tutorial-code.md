### Playground

https://composer-playground-next.mybluemix.net/

### モデルファイル　.cto

```
/**
 * 私の商品取引ネットワーク
 */
namespace org.acme.mynetwork

// Commodity = 商品
asset Commodity identified by tradingSymbol {
    o String tradingSymbol
    o String description
    o String mainExchange
    o Double quantity
    --> Trader owner
}

// Trader = トレーダー
participant Trader identified by tradeId {
    o String tradeId
    o String firstName
    o String lastName
}

// Trade = トレード
transaction Trade {
    --> Commodity commodity
    --> Trader newOwner
}
```


### トランザクションファイル .js

```
/**
 * あるトレーダーから他のトレーダーへの商品の取引を追跡する
 * @param {org.acme.mynetwork.Trade} trade - 処理される取引
 * @transaction
 */
async function tradeCommodity(trade) {
    trade.commodity.owner = trade.newOwner;
    let assetRegistry = await getAssetRegistry('org.acme.mynetwork.Commodity');
    await assetRegistry.update(trade.commodity);
}
```

### 新しいトレーダーを登録する



```
{
  "$class": "org.acme.mynetwork.Trader",
  "tradeId": "TRADER1",
  "firstName": "Jenny",
  "lastName": "Jones"
}
```

```
{
  "$class": "org.acme.mynetwork.Trader",
  "tradeId": "TRADER2",
  "firstName": "Amy",
  "lastName": "Williams"
}
```

### 新しい商品を登録する

```
{
  "$class": "org.acme.mynetwork.Commodity",
  "tradingSymbol": "ABC",
  "description": "Test commodity",
  "mainExchange": "Euronext",
  "quantity": 72.297,
  "owner": "resource:org.acme.mynetwork.Trader#TRADER1"
}
```

### 新しいトランザクションを登録する

```
{
  "$class": "org.acme.mynetwork.Trade",
  "commodity": "resource:org.acme.mynetwork.Commodity#ABC",
  "newOwner": "resource:org.acme.mynetwork.Trader#TRADER2"
}
```



### Queries

```

query selectCommodities {

  description: "Select all commodities"

  statement:

      SELECT org.acme.mynetwork.Commodity

}


query selectCommoditiesWithHighQuantity {

  description: "Select commodities based on quantity"

  statement:

      SELECT  org.acme.mynetwork.Commodity

          WHERE (quantity > 60)

}

```
